import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var hightwayView: UIImageView!
    @IBOutlet weak var carView: UIImageView!
    @IBOutlet weak var rockView: UIImageView!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var leftTopShrubConstrain: NSLayoutConstraint!
    @IBOutlet weak var rightTopShrubConstrain: NSLayoutConstraint!

    
    var stepMoveLeftShrubs:CGFloat = -500
    var stepMoveRightShrub:CGFloat = -50
    var oneStepCar:CGFloat = 20
    var rockPoint:CGFloat = -70
    
    var timer : Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.rockView.frame.origin.x = self.randomRock()
        self.moveLeftShrub()
        self.moveRightShrub()
        self.bumpWithRock()
    }
    
    func moveLeftShrub () {
        self.leftTopShrubConstrain.constant = UIScreen.main.bounds.height
        UIView.animate(withDuration: 3, animations: {
            self.view.layoutIfNeeded()
        }){ (_) in
            self.leftTopShrubConstrain.constant = self.stepMoveLeftShrubs
            // вызов в основном потоке
            DispatchQueue.main.asyncAfter(deadline: .now()+0.1, execute: {
                self.moveLeftShrub()
            })
        }
    }
    
    func moveRightShrub () {
        self.rightTopShrubConstrain.constant = UIScreen.main.bounds.height
        UIView.animate(withDuration: 3, animations: {
            self.view.layoutIfNeeded()
        }){ (_) in
            self.rightTopShrubConstrain.constant = self.stepMoveRightShrub
            DispatchQueue.main.asyncAfter(deadline: .now()+0.1, execute: {
                self.moveRightShrub()
            })
        }
    }
    
    func randomRock () -> CGFloat {
        let randomRock = Int.random(in: 0 ..< Int(self.hightwayView.frame.width - self.carView.frame.width))
        return CGFloat(randomRock)
        }
    
    func bumpWithRock () {
        timer = Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true) { (_) in
            self.rockView.frame.origin.y += 2
            if self.rockView.frame.origin.y == self.hightwayView.frame.height{
                self.updateRock()
                DispatchQueue.main.asyncAfter(deadline: .now()+0.1, execute: {
                    self.bumpWithRock()
                })
            }
            if self.rockView.frame.origin.y+self.rockView.frame.height >= self.carView.frame.origin.y{
                if self.rockView.frame.origin.x+self.rockView.frame.width < self.carView.frame.origin.x{
                }else{
                    if self.rockView.frame.origin.x > self.carView.frame.origin.x+self.carView.frame.width{
                    }else{
                        self.gameOver()
                        self.updateRock()
                    }
                }
            }
        }
    }
    
    func updateRock(){
        self.timer?.invalidate()
        self.timer = nil
        self.rockView.frame.origin.y = self.rockPoint
        self.rockView.frame.origin.x = self.randomRock()
    }
    
    func gameOver(){
        let alert = UIAlertController(title: "", message: "Game over", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
           self.navigationController?.popToRootViewController(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func leftButtonPresed(_ sender: UIButton) {
        if carView.frame.origin.x > oneStepCar {
            UIView.animate(withDuration: 0.3) {
                self.carView.frame.origin.x -= self.oneStepCar
            }
            return
            }
        UIView.animate(withDuration: 0.3) {
            self.carView.frame.origin.x = 0
        }
    }
    
    @IBAction func rightButtonPressed(_ sender: UIButton) {
        if carView.frame.origin.x < hightwayView.frame.width - (carView.frame.width + oneStepCar) {
            UIView.animate(withDuration: 0.3) {
                self.carView.frame.origin.x += self.oneStepCar
            }
            return
        }
                UIView.animate(withDuration: 0.3, animations: {
                    self.carView.frame.origin.x = self.hightwayView.frame.width - self.carView.frame.width
                })
            }
        }
    
    


