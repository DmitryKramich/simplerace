import UIKit

class MenuViewController: UIViewController {
    @IBOutlet weak var newGameButton: UIButton!
    @IBOutlet weak var highscoreTableButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func newGameButtonPressed(_ sender: UIButton) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }

    @IBAction func highscoreTableButtonPressed(_ sender: UIButton) {
    }
    
    @IBAction func settingsButtonPressed(_ sender: UIButton) {
    }
}
